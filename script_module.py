#%% Import

from all_functions import DiscreteSpaceInterval, stat_sol_complete, plot_complete_int_f, random_intial_data, randomly_perturbed_initial_data, stat_sol_well_perturbed, two_domain_wall, complete_sim, plotting_m1_from_saved_mag, update_frame, clear_frame, domain_wall_theta_star_build_function, pert_two_domain_wall_build
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import warnings
from functools import partial
import os

#%% Choices for the simulation

#% Choice of the initial data :
# 1 : random initial data (cos(random_theta), sin(random_theta)*cos(random_phi), sin(random_theta)*sin(random_phi)) with theta and phi random with law defined by law_choice
# 2 : stationary solution (cos(theta_h0), sin(theta_h0), 0)
# 3 : random perturbation of the stationary solution (cos(theta_h0 + eps_pert*random_theta), sin(theta_h0 + eps_pert*random_theta) * cos(eps_pert*random_phi), sin(theta_h0 + eps_pert*random_theta) * sin(eps_pert*random_phi))
# 4 : well-prepared perturbation of the stationary solution (see the article for a precise statement)
# 5 : 2-domain wall
# 6 : random perturbation of a 2-domain wall
# 7 : random perturbation of a domain wall (cos(theta_* + eps_pert*random_theta), sin(theta_* + eps_pert*random_theta) * cos(eps_pert*random_phi), sin(theta_* + eps_pert*random_theta) * sin(eps_pert*random_phi))

choice_initial_data = 6


# Choice of saving all the data for the frames
save = True


#% Choice of the method for the random perturbation if choice_initial_data = 1 or 3
# 'uniform' : uniform law on (-scale, scale) for random_theta and random_phi
# 'normal' : normal law with variance scale for random_theta and random_phi
# 'uniform_fourier_reg' : uniform law multiplied by |n|^{-reg} for the Fourier coefficients of random_theta and random_phi
# 'normal_fourier_reg' : normal law with variance scale multiplied by |n|^{-reg} for the Fourier coefficients of random_theta and random_phi
law_choice = "normal_fourier_reg"


#% Plot of the energy
# If True, the evolution of the energy will be plotted
plot_energy = True
# If True, the evolution of the modified energy will be plotted
plot_mod_energy = False


#% Plot m_1 for some times
# If True, m_1 will be plotted at the times given in time_plot_list
plot_some_times = True


#% Turn off the external magnetic field ?
# If True, turn off the external magnetic field in LLG (but still compute the stationary solution with h0 given in #LLG parameters)
turn_off_H_ext = False


#%% Parameters ###

## LLG parameters
# Intensity of the external magnetic field
h0 = 0.5
# Gilbert damping
alpha = 1 

## Space parameters
# Half-length of the interval (-L0, L0)
L0 = 30
# Number of discrete points per length unit
numb_pt_per_unit = 50 

N0 = int(numb_pt_per_unit * L0) # Total number of discrete points in space : 2*N0 + 1

## Time parameters
# Final time of the simulation
T_final = 10
# Time step of the simulation
dt = 0.00005


## Number of frames per second and time between each frame

N_frame = 100 # Number of displayed frames per second
T_frame = 1/N_frame # Time between each frame
N_iter_per_frame = 1/(dt*N_frame) # Number of scheme iteration between each frame
speed = 1 # Speed of the video which will be saved if save == True. If speed = 2, it will be twice faster.

# Precision of the time displayed in the title of the plot
precision = 2

# Precision for the resolution of the ODE
dx_max_ode = 1e-4


# Intensity of the perturbation (for choice -1, 1, 3 and 4)
eps_perturbation = 1

# Regularity of the random perturbation (for choice -1, 1 and 3) and scaling
reg = 2
reg_scale = 10

# Number of the figure where the plots will be displayed
fig_nb = 3

# Show the plots ?
plt_show = False

# Print time of frames in console to track progress of the simulation ?
print_time_frame = True

# Parameters for 2-DW (for choice 0)
sigma1 = 1
phi01 = 0
sigma2 = -1
phi02 = 0
L_separation = 10

if sigma1 == sigma2:
    raise ValueError('Error : sigma1 and sigma2 must be different')

# Parameters for a DW
sigma = 1
phi0 = 0

# Definition of time_plot_list : must be an array of numbers between 0 and T_final (if plot_some_times == True)
time_plot_list = [0, 2, 4, 6, 8, 10]















#%% Setting up the interval and the external magnetic field

# Construction of the interval
I0 = DiscreteSpaceInterval(L0, N0)

# CFL
if dt>I0.space_step**2 / 4:
    warnings.warn("CFL not satisfied : dt = "+str(dt)+", dx**2 / 2 = "+str(I0.space_step**2 / 4))
    #raise Exception("CFL not satisfied : eps*dt = "+str(eps*dt)+", dx**2 = "+str(dx**2))


# Construction of the applied magnetic field
Ha = h0*np.ones(2*N0+1)





#%% Computation of the stationary solution ###

if choice_initial_data < 5:
    theta_h0, dtheta_h0, w_h0 = stat_sol_complete(h0, I0, dx_max_ode)
    
    # Plot of theta and of cos theta (the first component of w_h0)
    plot_complete_int_f(I0, theta_h0, 1, xlab='$x$', ylab='Solution $\\theta$')
    plot_complete_int_f(I0, w_h0.m[:,0], 2, ylim=[-1.1,1.1], xlab='$x$', ylab='Solution $\\cos \\theta$')
    
    # Plot of the energy
    plot_complete_int_f(I0, dtheta_h0**2 - np.sin(theta_h0)**2 - 2*h0*(1-np.cos(theta_h0)), 3, xlab='Time', ylab='Energy')
#end if

#%% Computation of the domain wall

if choice_initial_data == 7:
    theta_star_fct = domain_wall_theta_star_build_function(sigma,phi0)
    theta_star = theta_star_fct(I0.complete_interval)
#end if

if choice_initial_data == 6:
    theta_star_fct_1 = domain_wall_theta_star_build_function(sigma1,phi01)
    theta_star_fct_2 = domain_wall_theta_star_build_function(sigma2,phi02)
    theta_star_1 = theta_star_fct_1(I0.complete_interval + L_separation)
    theta_star_2 = theta_star_fct_2(I0.complete_interval - L_separation)
#end if


#%% Choice of the initial data and making of the folder

# Check whether the specified path exists or not
if not os.path.exists('nanowire'):
    # Create a new directory because it does not exist
    os.makedirs('nanowire')

if choice_initial_data == 1:
    M0 = random_intial_data(I0)
    str_directory = 'nanowire/random'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'random_init'
elif choice_initial_data == 2:
    M0 = w_h0
    str_directory = 'nanowire/stat_sol'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'stat_sol'
elif choice_initial_data == 3:
    M0 = randomly_perturbed_initial_data(theta_h0, I0, eps_perturbation, law=law_choice, regularity=reg, regularity_scale=reg_scale)
    str_directory = 'nanowire/rand_pert_stat_sol'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'rand_pert_stat_sol'
elif choice_initial_data == 4:
    M0 = stat_sol_well_perturbed(h0, theta_h0, dtheta_h0, eps_perturbation, I0)
    str_directory = 'nanowire/well_pert_stat_sol'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'well_pert_stat_sol'
elif choice_initial_data == 5:
    M0 = two_domain_wall(I0, sigma1, phi01, sigma2, phi02, L_separation)
    str_directory = 'nanowire/2DW'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = '2DW'
elif choice_initial_data == 6:
    M0 = pert_two_domain_wall_build(I0, sigma1, phi01, sigma2, phi02, L_separation, eps_perturbation, law = law_choice, regularity = reg, regularity_scale = reg_scale)
    str_directory = 'nanowire/pert_2DW'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'pert_2DW'
elif choice_initial_data == 7:
    M0 = randomly_perturbed_initial_data(theta_star, I0, eps_perturbation, law=law_choice, regularity=reg, regularity_scale=reg_scale)
    str_directory = 'nanowire/pert_DW'
    isExist = os.path.exists(str_directory)
    if not isExist:
       os.makedirs(str_directory)
    #end if
    anim_file_name = 'pert_DW'


#%% If turn_off_H_ext == True : put Ha to 0 for the scheme

if turn_off_H_ext:
    Ha = 0
#end if


#%% If save == True : proceed the simulation by saving files

#% Proceed the simulation and save files

if save:
    complete_sim(M0, Ha, alpha, dt, T_final, N_iter_per_frame, print_time_frame, precision, str_directory, save)
#end if


#% Make the animation from the saved files
# If plot_energy == True : plot the evolution of the modified energy
# If plot_mod_energy == True : plot the evolution of the modified energy
# If plot_some_times == True : m_1 will be plotted at the times given in time_plot_list

if save:
    t_frame_list = np.load(str_directory+'/nanowire_times_frame.npy')
    fig = plt.figure(num=fig_nb)
    FFwriter = anim.FFMpegWriter(fps=speed*N_frame)
    complete_animation = anim.FuncAnimation(fig, partial(plotting_m1_from_saved_mag, Interv = I0, t_array = t_frame_list, figure = fig, string_directory = str_directory, precis = precision, plot_show=plt_show), frames = len(t_frame_list), interval = T_frame*1000, init_func = partial(clear_frame, fig_number = fig_nb))
    complete_animation.save(str_directory+'/'+anim_file_name + '.mp4', writer=FFwriter, dpi=800)
    
    fig2 = plt.figure(fig_nb+1)
    plotting_m1_from_saved_mag(0, I0, t_frame_list, fig2, str_directory, precision)
    plt.savefig(str_directory+'/nanowire_magnetization_initial_data.png')
    
    if plot_energy:
        fig3 = plt.figure(fig_nb+2, dpi=800)
        energy_array = np.load(str_directory+'/nanowire_modified_energy.npy')
        fig3.clear()
        ax3 = fig3.add_subplot()
        ax3.plot(t_frame_list, energy_array)
        ax3.set_xlabel('$t$')
        ax3.set_ylabel('$E_{h_0} (m(t))$')
    #end if
    
    if plot_mod_energy:
        fig3 = plt.figure(fig_nb+2, dpi=800)
        energy_array = np.load(str_directory+'/nanowire_modified_energy.npy')
        fig3.clear()
        ax3 = fig3.add_subplot()
        ax3.plot(t_frame_list, energy_array)
        ax3.set_xlabel('$t$')
        ax3.set_ylabel('$E_{h_0} (m(t))$')
    #end if
    
    if plot_some_times:
        fig4 = plt.figure(fig_nb + 3, dpi=800)
        fig4.clear()
        ax4 = fig4.add_subplot()
        ax4.set_xlabel('x')
        ax4.set_ylabel('$m_1 (t,x)$')
        ax4.set_ylim([-1.1, 1.1])
        for t in time_plot_list:
            idx = (np.abs(t_frame_list - t)).argmin()
            m_at_t = np.load(str_directory+'/nanowire_magnetization_'+str(idx)+'.npy')
            ax4.plot(I0.complete_interval, m_at_t[:,0], label='t = '+'{:.2f}'.format(t_frame_list[idx]))
        #end for
        
        ax4.legend()
        fig4.show()
    #end if
    
    def want_to_plot():
        """
        Interactive function to plot magnetization data at user-specified times.
    
        Prompts the user to input times for which they want to plot magnetization data.
        Loads magnetization data from saved files corresponding to the specified times and plots the data.
        """
        want_plot = True
        want_time_plot_list = []
        want_time_idx_plot_list = []
        nb_time_to_plot = 0
        while want_plot:
            t_plot = float(input('What time do you want to plot ? '))
            idx = (np.abs(t_frame_list - t_plot)).argmin()
            want_time_idx_plot_list.append(idx)
            want_time_plot_list.append(t_frame_list[idx])
            nb_time_to_plot += 1
            if input('Do you want to plot another time ? (y/n) ')=='n':
                want_plot = False
            #end if
        #end while
        fig5 = plt.figure(fig_nb + 4, dpi=800)
        fig5.clear()
        ax5 = fig5.add_subplot()
        ax5.set_xlabel('x')
        ax5.set_ylabel('$m_1 (t,x)$')
        for i in range(nb_time_to_plot):
            m_at_t = np.load(str_directory+'/nanowire_magnetization_'+str(want_time_idx_plot_list[i])+'.npy')
            ax5.plot(I0.complete_interval, m_at_t[:,0], label='t = '+'{:.2f}'.format(want_time_plot_list[i]))
        #end for
        
        ax5.legend()
        fig5.show()
    #end def
    
#end if



#%% If save == False : make the animation directly

if save == False:
    t_frame_list = [0]
    energy_list = [M0.modified_energy(Ha)]
    M = M0.copy()
    def update(i):
        global M, Ha, alpha, dt, T_frame, t_frame_list, energy_list, save
        update_frame(M, Ha, alpha, dt, T_frame, t_frame_list, energy_list, print_time_frame, precision, str_directory, save)
        plot_complete_int_f(I0, M.m [:, 0], fig_nb, ylim=[-1.1, 1.1], xlab='x', ylab='$m_1 (t,x)$', title='t = '+(('{:.'+str(precision)+'f}').format(t_frame_list[i])))
    #end def
    anim.FuncAnimation(fig_nb, update, frames = int(T_final*N_frame), interval = T_frame)
    



