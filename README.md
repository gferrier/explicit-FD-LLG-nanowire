# Explicit Finite Difference Scheme for LLG on a Nanowire

This project simulates the Landau-Lifshitz-Gilbert (LLG) equation on an infinite straight nanowire, with options for different initial data and the inclusion of an external magnetic field.

## Project Structure

- `all_functions.py`: Contains all the functions used in the simulation.
- `script_module.py`: Main script to set parameters, run simulations, and generate animations.

## Requirements

Ensure you have the following libraries installed:

- `numpy`
- `matplotlib`
- `scipy`
- `os`
- `functools`

You can install these using pip:

```bash
pip install numpy matplotlib scipy
```


## Parameters

### Choices for the Simulation

You can configure the initial data and other parameters in the `script_module.py` file:

- **Initial Data Options (`choice_initial_data`)**:
  - `0`: Two domain walls
  - `1`: Random initial data
  - `2`: Stationary solution
  - `3`: Random perturbation of the stationary solution
  - `4`: Well-prepared perturbation of the stationary solution

- **Saving Data (`save`)**: Set to `True` to save the magnetization data for each frame.

- **Random Perturbation Method (`law_choice`)**: Choose the method for random perturbations (`uniform`, `normal`, `uniform_fourier_reg`, `normal_fourier_reg`).

- **Energy Plotting**:
  - `plot_energy`: Plot the evolution of the energy.
  - `plot_mod_energy`: Plot the evolution of the modified energy.

- **Plotting Specific Times (`plot_some_times`)**: Plot `m_1` at specified times in `time_plot_list`.

- **External Magnetic Field**:
  - `turn_off_H_ext`: Set to `True` to turn off the external magnetic field in the LLG equation.

### Simulation Parameters

- **LLG Parameters**:
  - `h0`: Intensity of the external magnetic field.
  - `alpha`: Gilbert damping parameter.

- **Space Parameters**:
  - `L0`: Half-length of the interval (-L0, L0).
  - `numb_pt_per_unit`: Number of discrete points per unit length.

- **Time Parameters**:
  - `T_final`: Final time of the simulation.
  - `dt`: Time step of the simulation.

- **Frame Parameters**:
  - `N_frame`: Number of displayed frames per second.
  - `T_frame`: Time between each frame.
  - `N_iter_per_frame`: Number of scheme iterations between each frame.

## Running the Simulation

1. Set your desired parameters in the `script_module.py` file.
2. Run the main script to start the simulation:

```bash
python script_module.py
```

### Plotting and Animations

- If `save` is `True`, the script will save the simulation data and generate animations.
- You can interactively plot magnetization data at specific times using the `want_to_plot` function in the script.

### Example Usage

To simulate with the stationary solution as the initial data, set `choice_initial_data = 2` and run the script. The script will create a directory `nanowire/stat_sol` and save the simulation results there.

## License

This project is licensed under the MIT License. See the LICENSE file for details.

## Acknowledgements

This project is meant to be an improvment of the code generously shared by G. Carbou (LMAP, Université de Pau et des Pays de l'Adour).
